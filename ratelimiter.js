const redis = require('redis')
const redisClient = redis.createClient()
const moment = require('moment')

module.exports = (req,res,next) => {
    console.log("The IP is: " , req.ip);
    redisClient.on("error", function(error) {
        console.error(error);
    });
    redisClient.exists( req.ip ,(err,reply) => {
        if(err) {
            console.log("Redis not working...")
            next();
        }
        if(reply) {
            // user exists
            redisClient.get( req.ip , (err,Rdata) => {
                let data = JSON.parse(Rdata)
                console.log("Data retrieved from the redis: " , data);

                let currentTime = moment()
                console.log("currentTime" , currentTime);

                if(data.count >= 3) {
                    return res.status(429).json("Exceeded the maximum number of requests!")
                }

                // update the count and allow the request
                data.count++
                redisClient.set( req.ip , JSON.stringify(data) , 'EX' , 60);
                // allow request
                next()

            })
        } else {
            // add new user ip
            console.log("Expired or new User: the date now is : ", moment());
            let body = {
                'count': 1,
                'startTime': moment()
            }
            redisClient.set(req.ip ,JSON.stringify(body) , 'EX' , 60)
            next()
        }
    })
}