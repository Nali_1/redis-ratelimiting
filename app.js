const express = require('express')
const app = express()
const router = express.Router()
const rateCheck = require('./ratelimiter')



app.use(rateCheck)
app.use('/api',router)

app.get('/',(req,res) => {
  return res.send('<h1>API response</h1>')
});

app.listen(3000)