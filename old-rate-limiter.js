const redis = require('redis')
const redisClient = redis.createClient()
const moment = require('moment')

module.exports = (req,res,next) => {
    redisClient.exists( req.ip ,(err,reply) => {
        if(err) {
            console.log("Redis not working...")
            next();
        }
        if(reply) {
            // user exists
            // check time interval
            redisClient.get(req.ip,(err,reply) => {
                let data = JSON.parse(reply)
                console.log("el data aheh" , data.startTime);

                let currentTime = moment()
                //let difference = (currentTime - moment(data.startTime).unix())/60
                // if(difference >= 1) {
                //     let body = {
                //         'count': 1,
                //         'startTime': moment().unix()
                //     }
                //     redisClient.set(req.ip,JSON.stringify(body))
                //     // allow the request
                //     next()
                // }
                //  if(difference < 1) {
                if(data.count >= 3) {
                    return res.status(429).json("Exceeded the maximum number of requests!")
                }
                // update the count and allow the request
                data.count++
                redisClient.set(req.ip,JSON.stringify(data) , 'EX' , 60);
                // allow request
                next()
                //  }
            })
        } else {
            // add new user
            let body = {
                'count': 1,
                'startTime': moment()
            }
            redisClient.set(req.ip,JSON.stringify(body) , 'EX' , 60)
            next()
        }
    })
}