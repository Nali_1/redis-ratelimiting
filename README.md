# Redis installation:
### 1. brew install redis
### 2. Then Run the command redis-server

#### The server should start normally , but if an error was thrown **"Coonection refused - missing the config file"** Run this command instead:
**redis-server  /usr/local/etc/redis.conf**

#

# To Run the Application:
### 1. Clone the above repo.
### 2. After downloading it run: **npm i**
### 3. To start the server run : **node app**

#
# To produce error 429 "Too many requests":
### use postman or write **localhost:3000/** in your browser then refresh the page more than 3 times in one minute.

### you should see the error code 429 with "Too many requests msg".

### wait for 1 minute then try again , you should be able to see "API Response" text again.